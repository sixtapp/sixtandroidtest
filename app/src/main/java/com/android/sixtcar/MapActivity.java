package com.android.sixtcar;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.android.sixtcar.databinding.MapActivityBinding;
import com.android.sixtcar.model.CarModel;
import com.android.sixtcar.view.MapViewModel;

import java.util.ArrayList;

/**
 * Created by THKEYUR on 9/2/2017.
 *
 * Show map with car.
 */

public class MapActivity extends AppCompatActivity {

    private MapActivityBinding mapActivityBinding;
    private MapViewModel mapViewModel;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initBinding();
        setSupportActionBar(mapActivityBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initBinding() {

        mapActivityBinding = DataBindingUtil.setContentView(this, R.layout.map_activity);
        mapViewModel = new MapViewModel(this);
        mapActivityBinding.setMapViewModel(mapViewModel);

        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras().getBundle("bundle");
        ArrayList<CarModel> models = (ArrayList<CarModel>) bundle.getSerializable("list");
        mapViewModel.setList(models);

        mapViewModel.initMapView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        this.finish();

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapViewModel.destroy();
    }
}
