package com.android.sixtcar.model;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by THKEYUR on 9/1/2017.
 * <p>
 * Model class of car, mapped with remote server data.
 * Used to represent car information in list.
 */

public class CarModel implements Serializable {


    @SerializedName("id")
    public String id;
    @SerializedName("modelIdentifier")
    public String modelIdentifier;
    @SerializedName("modelName")
    public String modelName;
    @SerializedName("name")
    public String name;
    @SerializedName("make")
    public String make;
    @SerializedName("group")
    public String group;
    @SerializedName("color")
    public String color;
    @SerializedName("series")
    public String series;
    @SerializedName("fuelType")
    public String fuelType;
    @SerializedName("fuelLevel")
    public String fuelLevel;
    @SerializedName("transmission")
    public String transmission;
    @SerializedName("licensePlate")
    public String licensePlate;
    @SerializedName("latitude")
    public double latitude;
    @SerializedName("longitude")
    public double longitude;
    @SerializedName("innerCleanliness")
    public String innerCleanliness;
    @SerializedName("carImageUrl")
    public String carImageUrl;

    protected CarModel(Parcel in) {
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
        this.modelIdentifier = in.readString();
        this.make = in.readString();
        this.name = in.readString();
        this.group = in.readString();
        this.color = in.readString();
        this.series = in.readString();
        this.fuelLevel = in.readString();
        this.fuelType = in.readString();
        this.transmission = in.readString();
        this.licensePlate = in.readString();
        this.innerCleanliness = in.readString();
        this.carImageUrl = in.readString();
    }


/**
 * https://prod.drive-now-content.com/fileadmin/user_upload_global/assets/cars/{modelIdentifier}/{color}/2x/car.png*/


}
