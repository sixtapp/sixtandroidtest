package com.android.sixtcar;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.android.sixtcar.databinding.CarDetailActivityBinding;
import com.android.sixtcar.model.CarModel;
import com.android.sixtcar.view.CarDetailViewModel;

/**
 * Created by THKEYUR on 9/2/2017.
 */
public class CarDetailActivity extends AppCompatActivity {

    private static final String MODEL = "MODEL";

    private CarDetailActivityBinding carDetailActivityBinding;

    public static Intent launchDetail(Context context, CarModel model) {
        Intent intent = new Intent(context, CarDetailActivity.class);
        intent.putExtra(MODEL, model);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        carDetailActivityBinding =
                DataBindingUtil.setContentView(this, R.layout.car_detail_activity);
        setSupportActionBar(carDetailActivityBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getExtrasFromIntent();
    }


    private void getExtrasFromIntent() {
        CarModel carModel = (CarModel) getIntent().getSerializableExtra(MODEL);
        CarDetailViewModel carDetailViewModel = new CarDetailViewModel(carModel);
        carDetailActivityBinding.setCarDetailViewModel(carDetailViewModel);
        setTitle(carModel.name);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        this.finish();

        return super.onOptionsItemSelected(item);
    }
}
