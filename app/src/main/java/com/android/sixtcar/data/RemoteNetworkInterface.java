package com.android.sixtcar.data;

import com.android.sixtcar.model.CarModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by THKEYUR on 9/1/2017.
 * <p>
 * List of api call.
 */

public interface RemoteNetworkInterface {

    @GET("/cars.json")
    Call<List<CarModel>> getRemoteCarList();

}
