package com.android.sixtcar.data;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by THKEYUR on 9/1/2017.
 * <p>
 * Global retrofit object,
 * Retrofit type safe networking library for http netwerk call.
 * With Gson inbuilt feature, save time to parse json using keys manually.
 */

public class RemoteNetworkClient {

    private static Retrofit retrofit = null;
    private static OkHttpClient client;

    static {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

    }

    public static Retrofit getClient() {

        // create retrofit object to communicate with remote server

        retrofit = new Retrofit.Builder()
                .baseUrl("http://www.codetalk.de")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();


        return retrofit;
    }

}
