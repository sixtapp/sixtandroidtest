package com.android.sixtcar.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.android.sixtcar.R;
import com.android.sixtcar.databinding.CarItemBinding;
import com.android.sixtcar.model.CarModel;
import com.android.sixtcar.view.CarItemViewModel;

import java.util.Collections;
import java.util.List;

/**
 * Created by THKEYUR on 9/1/2017.
 * adapter class to display car in the list.
 */

public class CarListAdapter extends RecyclerView.Adapter<CarListAdapter.CarViewHolder> {

    private List<CarModel> carList;

    public CarListAdapter() {
        this.carList = Collections.emptyList();
    }

    @Override
    public CarViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CarItemBinding carBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.car_item,
                        parent, false);
        return new CarViewHolder(carBinding);
    }

    @Override
    public void onBindViewHolder(CarViewHolder holder, int position) {
        holder.bindCar(carList.get(position));
    }

    @Override
    public int getItemCount() {
        return carList.size();
    }

    public void setCarList(List<CarModel> carList) {
        this.carList = carList;
        notifyDataSetChanged();
    }

    public static class CarViewHolder extends RecyclerView.ViewHolder {
        CarItemBinding mCarItemBinding;

        public CarViewHolder(CarItemBinding carItem) {
            super(carItem.itemCar);
            this.mCarItemBinding = carItem;
        }

        void bindCar(CarModel car) {
            if (mCarItemBinding.getCarItemViewModel() == null) {
                mCarItemBinding.setCarItemViewModel(
                        new CarItemViewModel(car, itemView.getContext()));
            } else {
                mCarItemBinding.getCarItemViewModel().setCarModel(car);
            }
        }
    }
}
