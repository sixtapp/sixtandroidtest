package com.android.sixtcar;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.android.sixtcar.adapter.CarListAdapter;
import com.android.sixtcar.databinding.MainActivityBinding;
import com.android.sixtcar.model.CarModel;
import com.android.sixtcar.view.CarMainViewModel;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by THKEYUR on 9/2/2017.
 */
public class MainActivity extends AppCompatActivity implements Observer {


    private MainActivityBinding mainActivityBinding;
    private CarMainViewModel carMainViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initBinding();
        setSupportActionBar(mainActivityBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setupListView(mainActivityBinding.listCar);
        setupObserver(carMainViewModel);

    }

    /**
     * automate data binding to view using observer and DataBinding
     * To use it, Activity name and Layout name should be same
     */
    private void initBinding() {
        mainActivityBinding = DataBindingUtil.setContentView(this, R.layout.main_activity);
        carMainViewModel = new CarMainViewModel(this);
        mainActivityBinding.setMainCarViewModel(carMainViewModel);
    }

    /**
     * RecyclerView auto data binding
     */
    private void setupListView(RecyclerView v) {
        CarListAdapter adapter = new CarListAdapter();
        v.setAdapter(adapter);
        v.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_map) {

            if (carMainViewModel.getCarList().size() > 0) {

                Bundle bundle = new Bundle();
                bundle.putSerializable("list", (ArrayList<CarModel>) carMainViewModel.getCarList());

                Intent intent = new Intent(MainActivity.this, MapActivity.class);
                intent.putExtra("bundle", bundle);

                startActivity(intent);
            }
            return true;
        }
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void update(Observable observable, Object data) {
        if (observable instanceof CarMainViewModel) {
            CarListAdapter carListAdapter = (CarListAdapter) mainActivityBinding.listCar.getAdapter();
            CarMainViewModel carView = (CarMainViewModel) observable;
            carListAdapter.setCarList(carView.getList());
        }
    }

    private void setupObserver(Observable observable) {
        observable.addObserver(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        carMainViewModel.destroy();
    }
}
