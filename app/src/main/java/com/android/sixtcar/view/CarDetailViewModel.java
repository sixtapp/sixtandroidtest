package com.android.sixtcar.view;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.android.sixtcar.model.CarModel;
import com.android.sixtcar.utils.Utils;
import com.bumptech.glide.Glide;

/**
 * Created by THKEYUR on 9/2/2017.
 * <p>
 * car details model view - represents car details.
 */
public class CarDetailViewModel {
    private CarModel carModel;

    public CarDetailViewModel(CarModel model) {
        this.carModel = model;
    }


    public String getName() {
        return carModel.name;
    }

    public String getModelName() {
        return carModel.make;
    }

    public String getLicensePlate() {
        return carModel.licensePlate;
    }

    public String getTransmission() {
        return carModel.transmission;
    }

    public String getCarImageUrl() {
        return Utils.IMAGE_URL + carModel.modelIdentifier + "/" + carModel.color + Utils.IMAGE_URL_END;
    }

    public String getFuelType() {
        return carModel.fuelType;
    }

    @BindingAdapter({"imageUrl"})
    public static void loadImage(ImageView view, String imageUrl) {
        Glide.with(view.getContext()).load(imageUrl).into(view);
    }


}
