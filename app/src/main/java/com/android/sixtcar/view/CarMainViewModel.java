package com.android.sixtcar.view;

import android.content.Context;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.view.View;

import com.android.sixtcar.R;
import com.android.sixtcar.data.RemoteNetworkClient;
import com.android.sixtcar.data.RemoteNetworkInterface;
import com.android.sixtcar.model.CarModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by THKEYUR on 9/1/2017.
 * <p>
 * CarMainViewModel is responsible for initializing ui components.
 * Class will also do remote network operation using retrofit api.
 * Class will take care of any change in data and will update ui components accordingly.
 */

public class CarMainViewModel extends Observable {

    public ObservableInt progressBar;
    public ObservableInt recyclerView;
    public ObservableInt labelTextView;
    public ObservableField<String> messageLabel;

    public List<CarModel> getCarList() {
        return carList;
    }

    private List<CarModel> carList;
    private Context context;

    public CarMainViewModel(@NonNull Context context) {

        this.context = context;
        this.carList = new ArrayList<>();
        progressBar = new ObservableInt(View.GONE);
        recyclerView = new ObservableInt(View.GONE);
        labelTextView = new ObservableInt(View.VISIBLE);
        messageLabel = new ObservableField<>(context.getString(R.string.default_loading_text));
    }

    public List<CarModel> getList() {

        return carList;
    }

    public void onClickLabelTextView(View view) {

        progressBar.set(View.VISIBLE);
        labelTextView.set(View.GONE);
        RemoteNetworkInterface networkInterface = RemoteNetworkClient.getClient().create(RemoteNetworkInterface.class);


        Call<List<CarModel>> carModelCall = networkInterface.getRemoteCarList();
        carModelCall.enqueue(new Callback<List<CarModel>>() {
            @Override
            public void onResponse(Call<List<CarModel>> call, Response<List<CarModel>> response) {
                progressBar.set(View.GONE);
                labelTextView.set(View.GONE);
                recyclerView.set(View.VISIBLE);
                carList = response.body();

                setChanged(); // pre notify observer about data to be changes
                notifyObservers(); // call for data changes
            }

            @Override
            public void onFailure(Call<List<CarModel>> call, Throwable t) {
                progressBar.set(View.GONE);
                labelTextView.set(View.VISIBLE);
                recyclerView.set(View.GONE);
            }
        });

    }

    public void destroy() {
        context = null;
    }
}
