package com.android.sixtcar.view;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.view.View;
import android.widget.ImageView;

import com.android.sixtcar.CarDetailActivity;
import com.android.sixtcar.model.CarModel;
import com.android.sixtcar.utils.Utils;
import com.bumptech.glide.Glide;

/**
 * Created by THKEYUR on 9/1/2017.
 * <p>
 * CarItemViewModel is responsible for initializing ui components.
 */
public class CarItemViewModel extends BaseObservable {


    private CarModel carModel;

    public CarItemViewModel(CarModel car, Context context) {
        this.carModel = car;
    }

    @BindingAdapter("imageUrl")
    public static void setImageUrl(ImageView imageView, String url) {
        Glide.with(imageView.getContext()).load(url).into(imageView);
    }

    public String getCarName() {
        return carModel.name;
    }

    public String getCarBrand() {
        return carModel.make;
    }

    public String getCarTransmission() {
        return carModel.transmission;
    }

    public String getCarImage() {
        return Utils.IMAGE_URL + carModel.modelIdentifier + "/" + carModel.color + Utils.IMAGE_URL_END;
    }

    public void onItemClick(View view) {

        view.getContext().startActivity(CarDetailActivity.launchDetail(view.getContext(), carModel));

    }

    public void setCarModel(CarModel carModel) {
        this.carModel = carModel;
        notifyChange();
    }
}
