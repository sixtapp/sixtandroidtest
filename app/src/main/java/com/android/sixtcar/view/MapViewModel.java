package com.android.sixtcar.view;

import android.app.Activity;
import android.content.Context;
import android.databinding.BaseObservable;
import android.support.annotation.NonNull;

import com.android.sixtcar.R;
import com.android.sixtcar.model.CarModel;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by THKEYUR on 9/2/2017.
 * <p>
 * List out car in map.
 */

public class MapViewModel extends BaseObservable {

    private List<CarModel> carList;
    private Context context;


    public MapViewModel(@NonNull Context context) {

        this.context = context;
        this.carList = new ArrayList<CarModel>();
    }

    public void setList(ArrayList<CarModel> list) {
        carList.addAll(list);
    }

    public void initMapView() {

        MapFragment mapFrag = (MapFragment) ((Activity) context).getFragmentManager().findFragmentById(R.id.map);

        mapFrag.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap map) {

                CameraUpdate center =
                        CameraUpdateFactory.newLatLng(new LatLng(carList.get(0).latitude,
                                carList.get(0).longitude));
                CameraUpdate zoom = CameraUpdateFactory.zoomTo(12);

                map.moveCamera(center);
                map.animateCamera(zoom);
                addMarkers(map);
            }
        });

    }

    private void addMarkers(GoogleMap map) {

        int size = carList.size();

        for (int i = 0; i < size; i++) {
            CarModel carModel = carList.get(i);
            map.addMarker(new MarkerOptions().position(new LatLng(carModel.latitude,
                    carModel.longitude))
                    .title(carModel.name)
                    .snippet(carModel.make));
        }

    }

    public void destroy() {
        carList.clear();
        context = null;
    }


}
